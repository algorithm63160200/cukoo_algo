public class Cuckoo {

    static int MAXN = 11;
    static int ver = 2;
    private static int[] pos = new int[ver];
    private static int[][] hashtable = new int[ver][MAXN];

    public static void main(String[] args) {
        int input_cuckoo[] = { 20, 50, 53, 75, 100,
                67, 105, 3, 36, 39 };
        int n = input_cuckoo.length;
        System.out.println("intput Cuckoo: ");
        for (int i : input_cuckoo) {
            System.out.print(i+" ");
        }
        System.out.println();
        cuckoo(input_cuckoo, n);
    }

    private static void cuckoo(int keys[], int n) { // function Cuckoo
        initTable();
        for (int i = 0, cnt = 0; i < n; i++, cnt = 0)
            place(keys[i], 0, cnt, n);
            printTable();
    }

    private static void initTable() {

        for (int j = 0; j < MAXN; j++)
        for (int i = 0; i < ver; i++)
            hashtable[i][j] = Integer.MIN_VALUE;

    }

    private static void place(int key, int j, int cnt, int n) {
        if (cnt == n) {
            System.out.printf("%d unpositioned\n", key);
            System.out.printf("Cycle present. REHASH.\n");
            return;
        }
        for (int i = 0; i < ver; i++) {
            pos[i] = hash(i + 1, key);
            if (hashtable[i][pos[i]] == key)
                return;
        }

        if (hashtable[j][pos[j]] != Integer.MIN_VALUE) {
            int dis = hashtable[j][pos[j]];
            hashtable[j][pos[j]] = key;
            place(dis, (j + 1) % ver, cnt + 1, n);
        } else
            hashtable[j][pos[j]] = key;

    }

    private static int hash(int function, int key) { // function Hash
        switch (function) {
            case 1:
                return key % MAXN; // h1
            case 2:
                return (key / MAXN) % MAXN; // h2
        }
        return Integer.MIN_VALUE;
    }

    static void printTable() {
        System.out.printf("Final hash tables:\n");

        for (int i = 0; i < ver; i++, System.out.printf("\n"))
            for (int j = 0; j < MAXN; j++)
                if (hashtable[i][j] == Integer.MIN_VALUE)
                    System.out.printf("- ");
                else
                    System.out.printf("%d ", hashtable[i][j]);

        System.out.printf("\n");
    }
}
